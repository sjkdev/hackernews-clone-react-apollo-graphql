module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/container/NewsDetailWithData.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactApollo = __webpack_require__("react-apollo");

var _router = __webpack_require__("next/router");

var _router2 = _interopRequireDefault(_router);

var _NewsDetail = __webpack_require__("./components/presentational/NewsDetail.js");

var _NewsDetail2 = _interopRequireDefault(_NewsDetail);

var _hideNewsItem = __webpack_require__("./data/mutations/hideNewsItem.js");

var _hideNewsItem2 = _interopRequireDefault(_hideNewsItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _reactApollo.graphql)(_hideNewsItem2.default, {
  props: function props(_ref) {
    var ownProps = _ref.ownProps,
        mutate = _ref.mutate;
    return {
      hideNewsItem: function hideNewsItem(id) {
        return mutate({
          variables: { id: id }
        })
        // .then(() => Router.push(`/login?id=${id}&password=${password}`))
        .catch(function () {
          return _router2.default.push('/login', '/hide?id=' + id + '&how=up&goto=news');
        });
      },
      unhideNewsItem: function unhideNewsItem(id) {
        return mutate({
          variables: { id: id }
        }).catch(function () {
          return _router2.default.push('/login', '/unhide?id=' + id + '&how=up&goto=news');
        });
      }
    };
  }
})(_NewsDetail2.default);

/***/ }),

/***/ "./components/container/NewsFeedWithApolloRenderer.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__("babel-runtime/helpers/extends");

var _extends3 = _interopRequireDefault(_extends2);

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _NewsFeed = __webpack_require__("./components/presentational/NewsFeed.js");

var _NewsFeed2 = _interopRequireDefault(_NewsFeed);

var _LoadingSpinner = __webpack_require__("./components/presentational/LoadingSpinner.js");

var _LoadingSpinner2 = _interopRequireDefault(_LoadingSpinner);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
  var _ref$data = _ref.data,
      loading = _ref$data.loading,
      error = _ref$data.error,
      feed = _ref$data.feed,
      data = _ref.data,
      options = _ref.options;

  if (error) return _react2.default.createElement(
    'tr',
    null,
    _react2.default.createElement(
      'td',
      null,
      'Error loading news items.'
    )
  );
  if (feed && feed.length) return _react2.default.createElement(_NewsFeed2.default, (0, _extends3.default)({ newsItems: feed }, options));
  return _react2.default.createElement(_LoadingSpinner2.default, null);
};

/***/ }),

/***/ "./components/container/NewsTitleWithData.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactApollo = __webpack_require__("react-apollo");

var _router = __webpack_require__("next/router");

var _router2 = _interopRequireDefault(_router);

var _NewsTitle = __webpack_require__("./components/presentational/NewsTitle.js");

var _NewsTitle2 = _interopRequireDefault(_NewsTitle);

var _upvoteNewsItem = __webpack_require__("./data/mutations/upvoteNewsItem.js");

var _upvoteNewsItem2 = _interopRequireDefault(_upvoteNewsItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _reactApollo.graphql)(_upvoteNewsItem2.default, {
  props: function props(_ref) {
    var ownProps = _ref.ownProps,
        mutate = _ref.mutate;
    return {
      upvoteNewsItem: function upvoteNewsItem(id) {
        return mutate({
          variables: { id: id }
        })
        // .then(() => Router.push(`/login?id=${id}&password=${password}`))
        .catch(function () {
          return _router2.default.push('/login', '/vote?id=' + id + '&how=up&goto=news');
        });
      }
    };
  }
})(_NewsTitle2.default);

/***/ }),

/***/ "./components/presentational/Footer.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _link = __webpack_require__("next/link");

var _link2 = _interopRequireDefault(_link);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Footer = function Footer(props) {
  return _react2.default.createElement(
    'tr',
    null,
    _react2.default.createElement(
      'td',
      { style: { padding: '0px' } },
      _react2.default.createElement('img', { alt: '', src: '/static/s.gif', height: '10', width: '0' }),
      _react2.default.createElement(
        'table',
        { style: { height: '2px', width: '100%', borderSpacing: '0px' } },
        _react2.default.createElement(
          'tbody',
          null,
          _react2.default.createElement(
            'tr',
            null,
            _react2.default.createElement('td', { style: { backgroundColor: '#ff6600' } })
          )
        )
      ),
      _react2.default.createElement('br', null),
      _react2.default.createElement(
        'center',
        null,
        _react2.default.createElement(
          'span',
          { className: 'yclinks' },
          _react2.default.createElement(
            'a',
            { href: '/newsguidelines' },
            'Guidelines'
          ),
          '\xA0| ',
          _react2.default.createElement(
            _link2.default,
            { prefetch: true, href: '/newsfaq' },
            _react2.default.createElement(
              'a',
              null,
              'FAQ'
            )
          ),
          '\xA0| ',
          _react2.default.createElement(
            'a',
            { href: 'mailto:hn@ycombinator.com' },
            'Support'
          ),
          '\xA0| ',
          _react2.default.createElement(
            'a',
            { href: 'https://github.com/HackerNews/API' },
            'API'
          ),
          '\xA0| ',
          _react2.default.createElement(
            _link2.default,
            { prefetch: true, href: '/security' },
            _react2.default.createElement(
              'a',
              null,
              'Security'
            )
          ),
          '\xA0| ',
          _react2.default.createElement(
            _link2.default,
            { prefetch: true, href: '/lists' },
            _react2.default.createElement(
              'a',
              null,
              'Lists'
            )
          ),
          '\xA0| ',
          _react2.default.createElement(
            _link2.default,
            { prefetch: true, href: '/bookmarklet' },
            _react2.default.createElement(
              'a',
              null,
              'Bookmarklet'
            )
          ),
          '\xA0| ',
          _react2.default.createElement(
            _link2.default,
            { prefetch: true, href: '/dmca' },
            _react2.default.createElement(
              'a',
              null,
              'DMCA'
            )
          ),
          '\xA0| ',
          _react2.default.createElement(
            'a',
            { href: 'http://www.ycombinator.com/apply/' },
            'Apply to YC'
          ),
          '\xA0| ',
          _react2.default.createElement(
            'a',
            { href: 'mailto:hn@ycombinator.com' },
            'Contact'
          )
        ),
        _react2.default.createElement('br', null),
        _react2.default.createElement('br', null),
        _react2.default.createElement(
          'form',
          { method: 'get', action: '//hn.algolia.com/', style: { marginBottom: '1em' } },
          'Search:',
          _react2.default.createElement('input', { type: 'text', name: 'q', value: '', size: '17', autoCorrect: 'off', spellCheck: 'false', autoCapitalize: 'off', autoComplete: 'false' })
        )
      )
    )
  );
};

exports.default = Footer;

/***/ }),

/***/ "./components/presentational/Header.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _link = __webpack_require__("next/link");

var _link2 = _interopRequireDefault(_link);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _HeaderNav = __webpack_require__("./components/presentational/HeaderNav.js");

var _HeaderNav2 = _interopRequireDefault(_HeaderNav);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Header = function Header(props) {
  return _react2.default.createElement(
    'tr',
    null,
    _react2.default.createElement(
      'td',
      { style: { backgroundColor: '#ff6600', padding: '0px' } },
      _react2.default.createElement(
        'table',
        { style: { border: '0px', padding: '2px', borderSpacing: '0px', width: '100%' } },
        _react2.default.createElement(
          'tbody',
          null,
          _react2.default.createElement(
            'tr',
            null,
            _react2.default.createElement(
              'td',
              { style: { width: '18px', padding: '0px', paddingRight: '4px' } },
              _react2.default.createElement(
                _link2.default,
                { prefetch: true, href: '/' },
                _react2.default.createElement(
                  'a',
                  null,
                  _react2.default.createElement('img', { src: '/static/y18.gif', style: { width: '18px', height: '18px', border: '1px', borderColor: 'white', borderStyle: 'solid' } })
                )
              )
            ),
            _react2.default.createElement(
              'td',
              { style: { lineHeight: '12px', height: '10px', padding: '0px' } },
              _react2.default.createElement(_HeaderNav2.default, props)
            ),
            _react2.default.createElement(
              'td',
              { style: { textAlign: 'right', padding: '0px', paddingRight: '4px' } },
              props.me ? _react2.default.createElement(
                'span',
                { className: 'pagetop' },
                _react2.default.createElement(
                  _link2.default,
                  { prefetch: true, href: '/user?id=' + props.me.id },
                  _react2.default.createElement(
                    'a',
                    null,
                    props.me.id
                  )
                ),
                ' (' + props.me.karma + ') | ',
                _react2.default.createElement(
                  'a',
                  { href: '/logout?auth=d78ccc2c6120ffe08f32451519c2ff46d34c51ab&amp;goto=' + props.currentURL },
                  'logout'
                )
              ) : _react2.default.createElement(
                'span',
                { className: 'pagetop' },
                _react2.default.createElement(
                  _link2.default,
                  { prefetch: true, href: '/login?goto=' + props.currentURL },
                  _react2.default.createElement(
                    'a',
                    null,
                    'login'
                  )
                )
              )
            )
          )
        )
      )
    )
  );
};
Header.defaultProps = {
  me: null
};
Header.propTypes = {
  me: _propTypes2.default.shape({
    id: _propTypes2.default.string,
    karma: _propTypes2.default.number
  }),
  currentURL: _propTypes2.default.string.isRequired,
  isNavVisible: _propTypes2.default.bool.isRequired,
  title: _propTypes2.default.string.isRequired
};

exports.default = Header;

/***/ }),

/***/ "./components/presentational/HeaderNav.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _link = __webpack_require__("next/link");

var _link2 = _interopRequireDefault(_link);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var HeaderNav = function HeaderNav(props) {
  return props.isNavVisible ? _react2.default.createElement(
    'span',
    { className: 'pagetop' },
    _react2.default.createElement(
      'b',
      { className: 'hnname' },
      _react2.default.createElement(
        _link2.default,
        { prefetch: true, href: '/', as: '/news' },
        _react2.default.createElement(
          'a',
          null,
          props.title
        )
      )
    ),
    '\xA0',
    props.userId && _react2.default.createElement(
      _link2.default,
      { prefetch: true, href: '/newswelcome' },
      _react2.default.createElement(
        'a',
        null,
        'welcome'
      )
    ),
    props.userId && ' | ',
    _react2.default.createElement(
      _link2.default,
      { prefetch: true, href: '/newest' },
      _react2.default.createElement(
        'a',
        { className: props.currentURL === '/newest' ? 'topsel' : '' },
        'new'
      )
    ),
    props.userId && ' | ',
    props.userId && _react2.default.createElement(
      _link2.default,
      { prefetch: true, href: '/threads?id=' + props.userId },
      _react2.default.createElement(
        'a',
        { className: props.currentURL === '/threads' ? 'topsel' : '' },
        'threads'
      )
    ),
    ' | ',
    _react2.default.createElement(
      _link2.default,
      { prefetch: true, href: '/newcomments' },
      _react2.default.createElement(
        'a',
        { className: props.currentURL === '/newcomments' ? 'topsel' : '' },
        'comments'
      )
    ),
    ' | ',
    _react2.default.createElement(
      _link2.default,
      { prefetch: true, href: '/show' },
      _react2.default.createElement(
        'a',
        { className: props.currentURL === '/show' ? 'topsel' : '' },
        'show'
      )
    ),
    ' | ',
    _react2.default.createElement(
      _link2.default,
      { prefetch: true, href: '/ask' },
      _react2.default.createElement(
        'a',
        { className: props.currentURL === '/ask' ? 'topsel' : '' },
        'ask'
      )
    ),
    ' | ',
    _react2.default.createElement(
      _link2.default,
      { prefetch: true, href: '/jobs' },
      _react2.default.createElement(
        'a',
        { className: props.currentURL === '/jobs' ? 'topsel' : '' },
        'jobs'
      )
    ),
    ' | ',
    _react2.default.createElement(
      _link2.default,
      { prefetch: true, href: '/submit' },
      _react2.default.createElement(
        'a',
        { className: props.currentURL === '/submit' ? 'topsel' : '' },
        'submit'
      )
    ),
    props.currentURL === '/best' && ' | ',
    props.currentURL === '/best' && _react2.default.createElement(
      _link2.default,
      { prefetch: true, href: '/best' },
      _react2.default.createElement(
        'a',
        { className: 'topsel' },
        'best'
      )
    )
  ) : _react2.default.createElement(
    'span',
    { className: 'pagetop' },
    _react2.default.createElement(
      'b',
      null,
      props.title
    )
  );
};
HeaderNav.defaultProps = {
  userId: null
};
HeaderNav.propTypes = {
  userId: _propTypes2.default.string,
  currentURL: _propTypes2.default.string.isRequired,
  isNavVisible: _propTypes2.default.bool.isRequired,
  title: _propTypes2.default.string.isRequired
};

exports.default = HeaderNav;

/***/ }),

/***/ "./components/presentational/LoadingSpinner.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  return _react2.default.createElement(
    "tr",
    null,
    _react2.default.createElement(
      "td",
      null,
      _react2.default.createElement(
        "div",
        { className: "sk-circle" },
        _react2.default.createElement("div", { className: "sk-circle1 sk-child" }),
        _react2.default.createElement("div", { className: "sk-circle2 sk-child" }),
        _react2.default.createElement("div", { className: "sk-circle3 sk-child" }),
        _react2.default.createElement("div", { className: "sk-circle4 sk-child" }),
        _react2.default.createElement("div", { className: "sk-circle5 sk-child" }),
        _react2.default.createElement("div", { className: "sk-circle6 sk-child" }),
        _react2.default.createElement("div", { className: "sk-circle7 sk-child" }),
        _react2.default.createElement("div", { className: "sk-circle8 sk-child" }),
        _react2.default.createElement("div", { className: "sk-circle9 sk-child" }),
        _react2.default.createElement("div", { className: "sk-circle10 sk-child" }),
        _react2.default.createElement("div", { className: "sk-circle11 sk-child" }),
        _react2.default.createElement("div", { className: "sk-circle12 sk-child" })
      )
    )
  );
};

/***/ }),

/***/ "./components/presentational/NewsDetail.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = __webpack_require__("babel-runtime/helpers/taggedTemplateLiteral");

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _getPrototypeOf = __webpack_require__("babel-runtime/core-js/object/get-prototype-of");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__("babel-runtime/helpers/possibleConstructorReturn");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__("babel-runtime/helpers/inherits");

var _inherits3 = _interopRequireDefault(_inherits2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n      fragment NewsDetail on NewsItem {\n        id\n        commentCount\n        creationTime\n        hidden\n        submitterId\n        upvoteCount\n      }\n    '], ['\n      fragment NewsDetail on NewsItem {\n        id\n        commentCount\n        creationTime\n        hidden\n        submitterId\n        upvoteCount\n      }\n    ']);

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _graphqlTag = __webpack_require__("graphql-tag");

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

var _link = __webpack_require__("next/link");

var _link2 = _interopRequireDefault(_link);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _url = __webpack_require__("url");

var _url2 = _interopRequireDefault(_url);

var _convertNumberToTimeAgo = __webpack_require__("./helpers/convertNumberToTimeAgo.js");

var _convertNumberToTimeAgo2 = _interopRequireDefault(_convertNumberToTimeAgo);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NewsDetail = function (_Component) {
  (0, _inherits3.default)(NewsDetail, _Component);

  function NewsDetail() {
    (0, _classCallCheck3.default)(this, NewsDetail);
    return (0, _possibleConstructorReturn3.default)(this, (NewsDetail.__proto__ || (0, _getPrototypeOf2.default)(NewsDetail)).apply(this, arguments));
  }

  (0, _createClass3.default)(NewsDetail, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      return this.props.isJobListing ? _react2.default.createElement(
        'tr',
        null,
        _react2.default.createElement('td', { colSpan: '2' }),
        _react2.default.createElement(
          'td',
          { className: 'subtext' },
          _react2.default.createElement(
            'span',
            { className: 'age' },
            _react2.default.createElement(
              _link2.default,
              { prefetch: true, href: '/item?id=' + this.props.id },
              _react2.default.createElement(
                'a',
                null,
                (0, _convertNumberToTimeAgo2.default)(this.props.creationTime)
              )
            )
          )
        )
      ) : _react2.default.createElement(
        'tr',
        null,
        _react2.default.createElement('td', { colSpan: '2' }),
        _react2.default.createElement(
          'td',
          { className: 'subtext' },
          _react2.default.createElement(
            'span',
            { className: 'score' },
            this.props.upvoteCount,
            ' points'
          ),
          ' by ',
          _react2.default.createElement(
            _link2.default,
            { prefetch: true, href: '/user?id=' + this.props.submitterId },
            _react2.default.createElement(
              'a',
              { className: 'hnuser' },
              this.props.submitterId
            )
          ),
          ' ',
          _react2.default.createElement(
            'span',
            { className: 'age' },
            _react2.default.createElement(
              _link2.default,
              { prefetch: true, href: '/item?id=' + this.props.id },
              _react2.default.createElement(
                'a',
                null,
                (0, _convertNumberToTimeAgo2.default)(this.props.creationTime)
              )
            )
          ),
          ' | ',
          this.props.hidden ? _react2.default.createElement(
            'a',
            { href: 'javascript:void(0)', onClick: function onClick() {
                return _this2.props.hideNewsItem(_this2.props.id);
              } },
            'hide'
          ) : _react2.default.createElement(
            'a',
            { href: 'javascript:void(0)', onClick: function onClick() {
                return _this2.props.unhideNewsItem(_this2.props.id);
              } },
            'hide'
          ),
          this.props.isPostScrutinyVisible && _react2.default.createElement(
            'span',
            null,
            ' | ',
            _react2.default.createElement(
              'a',
              { href: 'https://hn.algolia.com/?query=Sublime%20Text%203.0&sort=byDate&dateRange=all&type=story&storyText=false&prefix&page=0' },
              'past'
            ),
            ' | ',
            _react2.default.createElement(
              'a',
              { href: 'https://www.google.com/search?q=Sublime%20Text%203.0' },
              'web'
            )
          ),
          ' | ',
          _react2.default.createElement(
            _link2.default,
            { prefetch: true, href: '/item?id=' + this.props.id },
            _react2.default.createElement(
              'a',
              null,
              function () {
                switch (_this2.props.commentCount) {
                  case 0:
                    return 'discuss';
                  case 1:
                    return '1 comment';
                  default:
                    return _this2.props.commentCount + ' comments';
                }
              }()
            )
          ),
          this.props.isFavoriteVisible && ' | favorite'
        )
      );
    }
  }]);
  return NewsDetail;
}(_react.Component);

NewsDetail.propTypes = {
  id: _propTypes2.default.number.isRequired,
  commentCount: _propTypes2.default.number.isRequired,
  creationTime: _propTypes2.default.number.isRequired,
  hidden: _propTypes2.default.bool.isRequired,
  hideNewsItem: _propTypes2.default.func.isRequired,
  isPostScrutinyVisible: _propTypes2.default.bool,
  isFavoriteVisible: _propTypes2.default.bool,
  isJobListing: _propTypes2.default.bool,
  submitterId: _propTypes2.default.string.isRequired,
  upvoteCount: _propTypes2.default.number.isRequired
};
NewsDetail.defaultProps = {
  isFavoriteVisible: true,
  isPostScrutinyVisible: false,
  isJobListing: false
};
NewsDetail.fragments = {
  newsItem: (0, _graphqlTag2.default)(_templateObject)
};
exports.default = NewsDetail;

/***/ }),

/***/ "./components/presentational/NewsFeed.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = __webpack_require__("babel-runtime/helpers/taggedTemplateLiteral");

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _extends2 = __webpack_require__("babel-runtime/helpers/extends");

var _extends3 = _interopRequireDefault(_extends2);

var _toConsumableArray2 = __webpack_require__("babel-runtime/helpers/toConsumableArray");

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    fragment NewsFeed on NewsItem {\n      id\n      hidden\n      ...NewsTitle\n      ...NewsDetail\n    }\n    ', '\n    ', '\n  '], ['\n    fragment NewsFeed on NewsItem {\n      id\n      hidden\n      ...NewsTitle\n      ...NewsDetail\n    }\n    ', '\n    ', '\n  ']);

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _graphqlTag = __webpack_require__("graphql-tag");

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

var _NewsTitleWithData = __webpack_require__("./components/container/NewsTitleWithData.js");

var _NewsTitleWithData2 = _interopRequireDefault(_NewsTitleWithData);

var _NewsDetailWithData = __webpack_require__("./components/container/NewsDetailWithData.js");

var _NewsDetailWithData2 = _interopRequireDefault(_NewsDetailWithData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NewsFeed = function NewsFeed(props) {
  var nextPage = Math.ceil((props.skip || 1) / props.first) + 1;

  var rows = [];
  if (props.notice) rows.push.apply(rows, (0, _toConsumableArray3.default)(props.notice));
  props.newsItems.forEach(function (newsItem, index) {
    if (!newsItem.hidden) {
      rows.push(_react2.default.createElement(_NewsTitleWithData2.default, (0, _extends3.default)({
        key: newsItem.id.toString() + 'title',
        isRankVisible: props.isRankVisible,
        isUpvoteVisible: props.isUpvoteVisible,
        rank: props.skip + index + 1
      }, newsItem)));
      rows.push(_react2.default.createElement(_NewsDetailWithData2.default, (0, _extends3.default)({
        key: newsItem.id.toString() + 'detail',
        isFavoriteVisible: false,
        isPostScrutinyVisible: props.isPostScrutinyVisible,
        isJobListing: props.isJobListing
      }, newsItem)));
      rows.push(_react2.default.createElement('tr', { className: 'spacer', key: newsItem.id.toString() + 'spacer', style: { height: 5 } }));
    }
  });
  rows.push(_react2.default.createElement('tr', { key: 'morespace', className: 'morespace', style: { height: '10px' } }));
  rows.push(_react2.default.createElement(
    'tr',
    { key: 'morelinktr' },
    _react2.default.createElement('td', { key: 'morelinkcolspan', colSpan: '2' }),
    _react2.default.createElement(
      'td',
      { key: 'morelinktd', className: 'title' },
      _react2.default.createElement(
        'a',
        { key: 'morelink', href: props.currentURL + '?p=' + nextPage, className: 'morelink', rel: 'nofollow' },
        'More'
      )
    )
  ));

  return _react2.default.createElement(
    'tr',
    null,
    _react2.default.createElement(
      'td',
      { style: { padding: '0px' } },
      _react2.default.createElement(
        'table',
        { style: { border: '0px', padding: '0px', borderCollapse: 'collapse', borderSpacing: '0px' }, className: 'itemlist' },
        _react2.default.createElement(
          'tbody',
          null,
          rows
        )
      )
    )
  );
};
NewsFeed.defaultProps = {
  isPostScrutinyVisible: false,
  isJobListing: false,
  isRankVisible: true,
  isUpvoteVisible: true,
  notice: null
};
NewsFeed.propTypes = {
  isPostScrutinyVisible: _propTypes2.default.bool,
  first: _propTypes2.default.number.isRequired,
  newsItems: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    id: _propTypes2.default.number.isRequired,
    commentCount: _propTypes2.default.number.isRequired,
    creationTime: _propTypes2.default.number.isRequired,
    hidden: _propTypes2.default.bool.isRequired,
    submitterId: _propTypes2.default.string.isRequired,
    title: _propTypes2.default.string.isRequired,
    text: _propTypes2.default.string,
    url: _propTypes2.default.string,
    upvoteCount: _propTypes2.default.number.isRequired
  })).isRequired,
  notice: _propTypes2.default.arrayOf(_propTypes2.default.element),
  skip: _propTypes2.default.number.isRequired,
  isJobListing: _propTypes2.default.bool,
  isRankVisible: _propTypes2.default.bool,
  isUpvoteVisible: _propTypes2.default.bool,
  currentURL: _propTypes2.default.string.isRequired
};
NewsFeed.fragments = {
  newsItem: (0, _graphqlTag2.default)(_templateObject, _NewsTitleWithData2.default.fragments.newsItem, _NewsDetailWithData2.default.fragments.newsItem)
};

exports.default = NewsFeed;

/***/ }),

/***/ "./components/presentational/NewsTitle.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = __webpack_require__("babel-runtime/helpers/taggedTemplateLiteral");

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _getPrototypeOf = __webpack_require__("babel-runtime/core-js/object/get-prototype-of");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__("babel-runtime/helpers/possibleConstructorReturn");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__("babel-runtime/helpers/inherits");

var _inherits3 = _interopRequireDefault(_inherits2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n      fragment NewsTitle on NewsItem {\n        id\n        title\n        url\n        upvoted\n      }\n    '], ['\n      fragment NewsTitle on NewsItem {\n        id\n        title\n        url\n        upvoted\n      }\n    ']);

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _graphqlTag = __webpack_require__("graphql-tag");

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

var _link = __webpack_require__("next/link");

var _link2 = _interopRequireDefault(_link);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _url = __webpack_require__("url");

var _url2 = _interopRequireDefault(_url);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NewsTitle = function (_Component) {
  (0, _inherits3.default)(NewsTitle, _Component);

  function NewsTitle() {
    (0, _classCallCheck3.default)(this, NewsTitle);
    return (0, _possibleConstructorReturn3.default)(this, (NewsTitle.__proto__ || (0, _getPrototypeOf2.default)(NewsTitle)).apply(this, arguments));
  }

  (0, _createClass3.default)(NewsTitle, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'tr',
        { className: 'athing', id: this.props.id },
        _react2.default.createElement(
          'td',
          { style: { textAlign: 'right', verticalAlign: 'top' }, className: 'title' },
          _react2.default.createElement(
            'span',
            { className: 'rank' },
            this.props.isRankVisible && this.props.rank + '.'
          )
        ),
        _react2.default.createElement(
          'td',
          { style: { verticalAlign: 'top' }, className: 'votelinks' },
          _react2.default.createElement(
            'center',
            null,
            this.props.isUpvoteVisible && _react2.default.createElement(
              'a',
              { className: this.props.upvoted ? 'nosee' : ' ', onClick: function onClick() {
                  return _this2.props.upvoteNewsItem(_this2.props.id);
                }, href: 'javascript:void(0)' },
              _react2.default.createElement('div', { className: 'votearrow', title: 'upvote' })
            )
          )
        ),
        _react2.default.createElement(
          'td',
          { className: 'title' },
          _react2.default.createElement(
            'a',
            { className: 'storylink', href: this.props.url ? this.props.url : 'item?id=' + this.props.id },
            this.props.title
          ),
          this.props.url && _react2.default.createElement(
            'span',
            { className: 'sitebit comhead' },
            ' (',
            _react2.default.createElement(
              'a',
              { href: 'from?site=' + _url2.default.parse(this.props.url).hostname },
              _react2.default.createElement(
                'span',
                { className: 'sitestr' },
                _url2.default.parse(this.props.url).hostname
              )
            ),
            ')'
          )
        )
      );
    }
  }]);
  return NewsTitle;
}(_react.Component);

NewsTitle.propTypes = {
  id: _propTypes2.default.number.isRequired,
  isRankVisible: _propTypes2.default.bool,
  isUpvoteVisible: _propTypes2.default.bool,
  rank: _propTypes2.default.number,
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string,
  upvoted: _propTypes2.default.bool.isRequired,
  upvoteNewsItem: _propTypes2.default.func.isRequired
};
NewsTitle.defaultProps = {
  isRankVisible: true,
  isUpvoteVisible: true,
  rank: undefined,
  url: undefined
};
NewsTitle.fragments = {
  newsItem: (0, _graphqlTag2.default)(_templateObject)
};
exports.default = NewsTitle;

/***/ }),

/***/ "./config.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var graphQLPath = exports.graphQLPath = '/graphql';
var graphiQLPath = exports.graphiQLPath = '/graphiql';

var dev = exports.dev = "development" !== 'production';
var appPath = exports.appPath =  false ? './build/app' : './src';

var HN_DB_URI = exports.HN_DB_URI = process.env.DB_URI || 'https://hacker-news.firebaseio.com';
var HN_API_VERSION = exports.HN_API_VERSION = process.env.HN_API_VERSION || '/v0';
var HN_API_URL = exports.HN_API_URL = process.env.HN_API_URL || '' + HN_DB_URI + HN_API_VERSION;

var HOST_NAME = exports.HOST_NAME = process.env.HOST_NAME || 'localhost';
var APP_PORT = exports.APP_PORT = process.env.APP_PORT || 3000;
var HOST = exports.HOST = process.browser && window.location.host || HOST_NAME + ':' + APP_PORT;

var APP_URI = exports.APP_URI = 'http://' + HOST;
var GRAPHQL_URL = exports.GRAPHQL_URL = '' + APP_URI + graphQLPath;
var GRAPHIQL_URL = exports.GRAPHIQL_URL = '' + APP_URI + graphiQLPath;

/*
  Cryptography
  https://nodejs.org/api/crypto.html#crypto_crypto_pbkdf2_password_salt_iterations_keylen_digest_callback
*/
var passwordIterations = exports.passwordIterations = 10000;

/***/ }),

/***/ "./data/mutations/hideNewsItem.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = __webpack_require__("babel-runtime/helpers/taggedTemplateLiteral");

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  mutation HideNewsItem($id: Int!) {\n    hideNewsItem(id: $id) {\n      id\n      hidden\n    }\n  }\n'], ['\n  mutation HideNewsItem($id: Int!) {\n    hideNewsItem(id: $id) {\n      id\n      hidden\n    }\n  }\n']);

var _graphqlTag = __webpack_require__("graphql-tag");

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _graphqlTag2.default)(_templateObject);

/***/ }),

/***/ "./data/mutations/upvoteNewsItem.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = __webpack_require__("babel-runtime/helpers/taggedTemplateLiteral");

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  mutation UpvoteNewsItem($id: Int!) {\n    upvoteNewsItem(id: $id) {\n      id\n      upvoteCount\n      upvoted\n    }\n  }\n'], ['\n  mutation UpvoteNewsItem($id: Int!) {\n    upvoteNewsItem(id: $id) {\n      id\n      upvoteCount\n      upvoted\n    }\n  }\n']);

var _graphqlTag = __webpack_require__("graphql-tag");

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _graphqlTag2.default)(_templateObject);

/***/ }),

/***/ "./data/queries/meQuery.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = __webpack_require__("babel-runtime/helpers/taggedTemplateLiteral");

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  query User {\n    me {\n      id\n      karma\n    }\n  }\n'], ['\n  query User {\n    me {\n      id\n      karma\n    }\n  }\n']);

var _graphqlTag = __webpack_require__("graphql-tag");

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _graphqlTag2.default)(_templateObject);

/***/ }),

/***/ "./helpers/convertNumberToTimeAgo.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (number) {
  var now = +new Date();
  var timeAgo = now - number;

  var ONE_YEAR = 3.154e+10;
  var ONE_MONTH = 2.628e+9;
  // don't care about weeks
  var ONE_DAY = 8.64e+7;
  var ONE_HOUR = 3.6e+6;
  var ONE_MINUTE = 60000;

  switch (true) {
    case timeAgo >= ONE_YEAR * 2:
      return Math.floor(timeAgo / ONE_YEAR) + ' years ago';
    case timeAgo >= ONE_YEAR:
      return 'a year ago';
    case timeAgo >= ONE_MONTH * 2:
      return Math.floor(timeAgo / ONE_MONTH) + ' months ago';
    case timeAgo >= ONE_MONTH:
      return '1 month ago';
    case timeAgo >= ONE_DAY * 2:
      return Math.floor(timeAgo / ONE_DAY) + ' days ago';
    case timeAgo >= ONE_DAY:
      return '1 day ago';
    case timeAgo >= ONE_HOUR * 2:
      return Math.floor(timeAgo / ONE_HOUR) + ' hours ago';
    case timeAgo >= ONE_HOUR:
      return '1 hour ago';
    case timeAgo >= ONE_MINUTE * 2:
      return Math.floor(timeAgo / ONE_MINUTE) + ' minutes ago';
    case timeAgo >= 0:
      return '1 minute ago';
    default:
      // timeAgo < 0 is in the future
      throw new Error('convertNumberToTimeAgo: number ' + number + ' timeAgo ' + timeAgo + ', is date older than 1970 or in the future?');
  }
};

/***/ }),

/***/ "./helpers/initApollo.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = initApollo;

var _apolloClient = __webpack_require__("apollo-client");

var _apolloLinkHttp = __webpack_require__("apollo-link-http");

var _apolloCacheInmemory = __webpack_require__("apollo-cache-inmemory");

var _isomorphicFetch = __webpack_require__("isomorphic-fetch");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _debug = __webpack_require__("debug");

var _debug2 = _interopRequireDefault(_debug);

var _config = __webpack_require__("./config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var logger = (0, _debug2.default)('app:initApollo');
logger.log = console.log.bind(console);

var apolloClient = null;

// Polyfill fetch() on the server (used by apollo-client)
if (!process.browser) {
  global.fetch = _isomorphicFetch2.default;
}

function create(initialState, _ref) {
  var getToken = _ref.getToken;

  return new _apolloClient.ApolloClient({
    ssrMode: !process.browser, // Disables forceFetch on the server (so queries are only run once)
    link: (0, _apolloLinkHttp.createHttpLink)({
      uri: _config.GRAPHQL_URL,
      credentials: 'same-origin',
      headers: {
        // HTTP Header:  Cookie: <cookiename>=<cookievalue>
        Cookie: 'connect.sid=' + getToken()['connect.sid']
      }
    }),
    cache: new _apolloCacheInmemory.InMemoryCache().restore(initialState || {}),
    connectToDevTools: process.browser
  });
}

function initApollo(initialState, options) {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (!process.browser) {
    return create(initialState, options);
  }

  // Reuse client on the client-side
  if (!apolloClient) {
    apolloClient = create(initialState, options);
  }

  return apolloClient;
}

/***/ }),

/***/ "./helpers/withData.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__("babel-runtime/core-js/object/get-prototype-of");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _regenerator = __webpack_require__("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__("babel-runtime/helpers/extends");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__("babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = __webpack_require__("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = __webpack_require__("babel-runtime/helpers/possibleConstructorReturn");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _createClass2 = __webpack_require__("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

var _inherits2 = __webpack_require__("babel-runtime/helpers/inherits");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _reactApollo = __webpack_require__("react-apollo");

var _cookie = __webpack_require__("cookie");

var _cookie2 = _interopRequireDefault(_cookie);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _debug = __webpack_require__("debug");

var _debug2 = _interopRequireDefault(_debug);

var _initApollo = __webpack_require__("./helpers/initApollo.js");

var _initApollo2 = _interopRequireDefault(_initApollo);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var logger = (0, _debug2.default)('app:withData');
logger.log = console.log.bind(console);

function parseCookies() {
  var ctx = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var mycookie = _cookie2.default.parse(ctx.req && ctx.req.headers.cookie ? ctx.req.headers.cookie : '', // document.cookie,
  options);
  logger('Parsing cookie: ', mycookie);
  return mycookie;
}

exports.default = function (ComposedComponent) {
  var _class, _temp;

  return _temp = _class = function (_React$Component) {
    (0, _inherits3.default)(WithData, _React$Component);
    (0, _createClass3.default)(WithData, null, [{
      key: 'getInitialProps',
      value: function () {
        var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(context) {
          var serverState, apollo, composedInitialProps, url, app;
          return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  serverState = { apollo: {} };

                  // Setup a server-side one-time-use apollo client for initial props and
                  // rendering (on server)

                  logger('getInitialProps with context: ', context);
                  apollo = (0, _initApollo2.default)({}, {
                    getToken: function getToken() {
                      return parseCookies(context);
                    } // ['connect.sid'], // .token,
                  });

                  // Evaluate the composed component's getInitialProps()

                  composedInitialProps = {};

                  if (!ComposedComponent.getInitialProps) {
                    _context.next = 8;
                    break;
                  }

                  _context.next = 7;
                  return ComposedComponent.getInitialProps(context, apollo);

                case 7:
                  composedInitialProps = _context.sent;

                case 8:
                  if (process.browser) {
                    _context.next = 16;
                    break;
                  }

                  if (!(context.res && context.res.finished)) {
                    _context.next = 11;
                    break;
                  }

                  return _context.abrupt('return');

                case 11:

                  // Provide the `url` prop data in case a GraphQL query uses it
                  url = { query: context.query, pathname: context.pathname };

                  // Run all GraphQL queries

                  app = _react2.default.createElement(
                    _reactApollo.ApolloProvider,
                    { client: apollo },
                    _react2.default.createElement(ComposedComponent, (0, _extends3.default)({ url: url }, composedInitialProps))
                  );
                  _context.next = 15;
                  return (0, _reactApollo.getDataFromTree)(app);

                case 15:

                  serverState = {
                    apollo: { // Make sure to only include Apollo's data state
                      data: apollo.cache.extract() // Extract query data from the Apollo's store
                    }
                  };

                case 16:
                  return _context.abrupt('return', (0, _extends3.default)({
                    serverState: serverState
                  }, composedInitialProps));

                case 17:
                case 'end':
                  return _context.stop();
              }
            }
          }, _callee, this);
        }));

        function getInitialProps(_x3) {
          return _ref.apply(this, arguments);
        }

        return getInitialProps;
      }()
    }]);

    function WithData(props) {
      (0, _classCallCheck3.default)(this, WithData);

      // Note: Apollo should never be used on the server side beyond the initial
      // render within `getInitialProps()` above (since the entire prop tree
      // will be initialized there), meaning the below will only ever be
      // executed on the client.
      var _this = (0, _possibleConstructorReturn3.default)(this, (WithData.__proto__ || (0, _getPrototypeOf2.default)(WithData)).call(this, props));

      _this.apollo = (0, _initApollo2.default)(_this.props.serverState.apollo.data, {
        getToken: function getToken() {
          return parseCookies();
        } // ['connect.sid'],
      });
      return _this;
    }

    (0, _createClass3.default)(WithData, [{
      key: 'render',
      value: function render() {
        return _react2.default.createElement(
          _reactApollo.ApolloProvider,
          { client: this.apollo },
          _react2.default.createElement(ComposedComponent, this.props)
        );
      }
    }]);
    return WithData;
  }(_react2.default.Component), _class.displayName = 'WithData(' + ComposedComponent.displayName + ')', _class.propTypes = {
    serverState: _propTypes2.default.object.isRequired
  }, _temp;
};

/***/ }),

/***/ "./layouts/Main.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _head = __webpack_require__("next/head");

var _head2 = _interopRequireDefault(_head);

var _reactApollo = __webpack_require__("react-apollo");

var _Header = __webpack_require__("./components/presentational/Header.js");

var _Header2 = _interopRequireDefault(_Header);

var _Footer = __webpack_require__("./components/presentational/Footer.js");

var _Footer2 = _interopRequireDefault(_Footer);

var _meQuery = __webpack_require__("./data/queries/meQuery.js");

var _meQuery2 = _interopRequireDefault(_meQuery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Main = function Main(props) {
  return _react2.default.createElement(
    'center',
    null,
    _react2.default.createElement(
      _head2.default,
      null,
      _react2.default.createElement(
        'title',
        null,
        'Hacker News Clone'
      ),
      _react2.default.createElement('meta', { name: 'referrer', content: 'origin' }),
      _react2.default.createElement('meta', { name: 'viewport', content: 'initial-scale=1.0, width=device-width' }),
      _react2.default.createElement('link', { rel: 'stylesheet', type: 'text/css', href: '/static/news.css' }),
      _react2.default.createElement('link', { rel: 'shortcut icon', href: '/static/favicon.ico' })
    ),
    _react2.default.createElement(
      'table',
      { id: 'hnmain', style: { border: '0px', padding: '0px', borderSpacing: '0px', borderCollapse: 'collapse', width: '85%', backgroundColor: '#f6f6ef' } },
      _react2.default.createElement(
        'tbody',
        null,
        _react2.default.createElement(_Header2.default, {
          title: props.title,
          isNavVisible: props.isNavVisible,
          isUserVisible: props.isUserVisible,
          me: props.me,
          currentURL: props.currentURL
        }),
        _react2.default.createElement('tr', { style: { height: '10px' } }),
        props.children,
        props.isFooterVisible && _react2.default.createElement(_Footer2.default, null)
      )
    )
  );
};
Main.defaultProps = {
  isFooterVisible: true,
  isNavVisible: true,
  isUserVisible: true,
  title: 'DWA News',
  me: null
};
Main.propTypes = {
  children: _propTypes2.default.node.isRequired,
  isNavVisible: _propTypes2.default.bool,
  isUserVisible: _propTypes2.default.bool,
  isFooterVisible: _propTypes2.default.bool,
  title: _propTypes2.default.string,
  me: _propTypes2.default.shape({
    id: _propTypes2.default.string,
    karma: _propTypes2.default.number
  }),
  currentURL: _propTypes2.default.string.isRequired
};

exports.default = (0, _reactApollo.graphql)(_meQuery2.default, {
  options: {
    // fetchPolicy: 'cache-and-network',
    // ssr: false,
  },
  props: function props(_ref) {
    var me = _ref.data.me;
    return {
      me: me
    };
  }
})(Main);

/***/ }),

/***/ "./pages/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _toConsumableArray2 = __webpack_require__("babel-runtime/helpers/toConsumableArray");

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _assign = __webpack_require__("babel-runtime/core-js/object/assign");

var _assign2 = _interopRequireDefault(_assign);

var _taggedTemplateLiteral2 = __webpack_require__("babel-runtime/helpers/taggedTemplateLiteral");

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  query topNewsItems($type: FeedType!, $first: Int!, $skip: Int!) {\n    feed(type: $type, first: $first, skip: $skip) {\n      ...NewsFeed\n    }\n  }\n  ', '\n'], ['\n  query topNewsItems($type: FeedType!, $first: Int!, $skip: Int!) {\n    feed(type: $type, first: $first, skip: $skip) {\n      ...NewsFeed\n    }\n  }\n  ', '\n']);

var _react = __webpack_require__("react");

var _react2 = _interopRequireDefault(_react);

var _reactApollo = __webpack_require__("react-apollo");

var _graphqlTag = __webpack_require__("graphql-tag");

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

var _propTypes = __webpack_require__("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Main = __webpack_require__("./layouts/Main.js");

var _Main2 = _interopRequireDefault(_Main);

var _NewsFeed = __webpack_require__("./components/presentational/NewsFeed.js");

var _NewsFeed2 = _interopRequireDefault(_NewsFeed);

var _NewsFeedWithApolloRenderer = __webpack_require__("./components/container/NewsFeedWithApolloRenderer.js");

var _NewsFeedWithApolloRenderer2 = _interopRequireDefault(_NewsFeedWithApolloRenderer);

var _withData = __webpack_require__("./helpers/withData.js");

var _withData2 = _interopRequireDefault(_withData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var POSTS_PER_PAGE = 30;

var query = (0, _graphqlTag2.default)(_templateObject, _NewsFeed2.default.fragments.newsItem);

var TopNewsFeed = (0, _reactApollo.graphql)(query, {
  options: function options(_ref) {
    var _ref$options = _ref.options,
        first = _ref$options.first,
        skip = _ref$options.skip;
    return {
      variables: {
        type: 'TOP',
        first: first,
        skip: skip
      }
    };
  },
  props: function props(_ref2) {
    var data = _ref2.data;
    return {
      data: data
    };
  },
  loadMorePosts: function loadMorePosts(data) {
    return data.fetchMore({
      variables: {
        skip: data.allNewsItems.length
      },
      updateQuery: function updateQuery(previousResult, _ref3) {
        var fetchMoreResult = _ref3.fetchMoreResult;

        if (!fetchMoreResult) {
          return previousResult;
        }
        return (0, _assign2.default)({}, previousResult, {
          // Append the new posts results to the old one
          allNewsItems: [].concat((0, _toConsumableArray3.default)(previousResult.allNewsItems), (0, _toConsumableArray3.default)(fetchMoreResult.allNewsItems))
        });
      }
    });
  }
})(_NewsFeedWithApolloRenderer2.default);

exports.default = (0, _withData2.default)(function (props) {
  var pageNumber = props.url.query && +props.url.query.p || 0;
  return _react2.default.createElement(
    _Main2.default,
    { currentURL: props.url.pathname },
    _react2.default.createElement(TopNewsFeed, { options: {
        currentURL: props.url.pathname,
        first: POSTS_PER_PAGE,
        skip: POSTS_PER_PAGE * pageNumber
      }
    })
  );
});

/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./pages/index.js");


/***/ }),

/***/ "apollo-cache-inmemory":
/***/ (function(module, exports) {

module.exports = require("apollo-cache-inmemory");

/***/ }),

/***/ "apollo-client":
/***/ (function(module, exports) {

module.exports = require("apollo-client");

/***/ }),

/***/ "apollo-link-http":
/***/ (function(module, exports) {

module.exports = require("apollo-link-http");

/***/ }),

/***/ "babel-runtime/core-js/object/assign":
/***/ (function(module, exports) {

module.exports = require("babel-runtime/core-js/object/assign");

/***/ }),

/***/ "babel-runtime/core-js/object/get-prototype-of":
/***/ (function(module, exports) {

module.exports = require("babel-runtime/core-js/object/get-prototype-of");

/***/ }),

/***/ "babel-runtime/helpers/asyncToGenerator":
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/asyncToGenerator");

/***/ }),

/***/ "babel-runtime/helpers/classCallCheck":
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/classCallCheck");

/***/ }),

/***/ "babel-runtime/helpers/createClass":
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/createClass");

/***/ }),

/***/ "babel-runtime/helpers/extends":
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/extends");

/***/ }),

/***/ "babel-runtime/helpers/inherits":
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/inherits");

/***/ }),

/***/ "babel-runtime/helpers/possibleConstructorReturn":
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/possibleConstructorReturn");

/***/ }),

/***/ "babel-runtime/helpers/taggedTemplateLiteral":
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/taggedTemplateLiteral");

/***/ }),

/***/ "babel-runtime/helpers/toConsumableArray":
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/toConsumableArray");

/***/ }),

/***/ "babel-runtime/regenerator":
/***/ (function(module, exports) {

module.exports = require("babel-runtime/regenerator");

/***/ }),

/***/ "cookie":
/***/ (function(module, exports) {

module.exports = require("cookie");

/***/ }),

/***/ "debug":
/***/ (function(module, exports) {

module.exports = require("debug");

/***/ }),

/***/ "graphql-tag":
/***/ (function(module, exports) {

module.exports = require("graphql-tag");

/***/ }),

/***/ "isomorphic-fetch":
/***/ (function(module, exports) {

module.exports = require("isomorphic-fetch");

/***/ }),

/***/ "next/head":
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "next/link":
/***/ (function(module, exports) {

module.exports = require("next/link");

/***/ }),

/***/ "next/router":
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "prop-types":
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ "react":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-apollo":
/***/ (function(module, exports) {

module.exports = require("react-apollo");

/***/ }),

/***/ "url":
/***/ (function(module, exports) {

module.exports = require("url");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map